#!/bin/bash
inkscape -z -e \
  ${1%.*}.png \
  -w 1024 -h 1024 \
  $1
