# How do businesses approach, measurer & report sustainability

#### SG3133 Group 4

<div align="right">

 Xiaolei Bian

 Rebecka Engström

 Ashwin Vishnu Mohanan

 Joanna Polak
 
</div>


# ![](https://www.kth.se/polopoly_fs/1.77257!/KTH_Logotyp_RGB_2013-2.svg)

---

# **Part 1**: Background
###  “what we have read”
---
## What is the purpose of creating these reports?
---
## What do these report contain?
---
## What companies did we look at? What business do they do?
---
# **Part 2**: Tools 
### for measuring and reporting sustainability in business
---
## GRI (explain)
---
## UN Global Compact Principles (explain)
---
## UN SDGs (does not need to be explained):
---
# **Part 3**: Examples
### How do Nokia and Sandvik use these tools (GRI GCP SDGs)
---
## Examples Nokia (screendumps)
---
## Examples Sandvik
---
## Summary of what’s reported
---
# **Part 4**: How reliable are these tools?
---
## What do they measure?
---
## Do they measure what’s relevant?
---
## How precise are they?
---
# **Part 5**: Conclusion
